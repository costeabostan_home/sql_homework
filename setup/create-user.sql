ALTER SESSION SET CONTAINER=PDB;
CREATE USER homework IDENTIFIED BY homework;
GRANT CREATE SESSION TO homework;
GRANT CREATE TABLE, CREATE VIEW, CREATE SEQUENCE, CREATE SYNONYM TO homework;
GRANT CREATE PROCEDURE TO homework;
GRANT CREATE TRIGGER TO homework;
GRANT UNLIMITED TABLESPACE TO homework;

SELECT * FROM user_users;